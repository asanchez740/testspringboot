
package com.example.dominio;

import lombok.Data;

@Data 
public class Persona {
    private String nombre;
    private String apellido;
    private String correo;
    private String tlf;      
    private Integer edad;
}
