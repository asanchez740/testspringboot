package com.example.holaSprig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaSprigApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolaSprigApplication.class, args);
	}

}
