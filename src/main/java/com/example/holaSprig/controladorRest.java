
package com.example.holaSprig;

import com.example.dominio.Persona;
import java.util.List;

import java.util.ArrayList;
import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@Slf4j

public class controladorRest {
    @Value("${indice.hola2}")
    private String hola2;
    
    @GetMapping("/") 
    
    public String inicio(Model model){

        String hola = "Estamos probando msj";
        
        Persona persona = new Persona();
        persona.setNombre("Argel");
        persona.setApellido("sanchez");
        persona.setCorreo("arge@gmail.com");
        persona.setTlf("+2323232");
        persona.setEdad(15);
        
        Persona persona2 = new Persona();
        persona2.setNombre("Diego");
        persona2.setApellido("Solf");
        persona2.setCorreo("ds@gmail.com");
        persona2.setTlf("+11111");
        persona2.setEdad(20);
        //List<Persona> clientes = new ArrayList();
        //clientes.add(persona);
        //clientes.add(persona2);
        
        
        List clientes = Arrays.asList(persona,persona2);
        
        
        
        log.info("Estoy ejecutando el controlador MVC");
        
        model.addAttribute("hola",hola);
        model.addAttribute("persona", persona);
        model.addAttribute("clientes", clientes);
        return "indice";
        
    }

}
